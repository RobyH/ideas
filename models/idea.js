'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Voto = require('./voto')

const IdeaSchema = Schema({
	descripcion: { type: String, required: true },
	autorId: { type: Number, required: true },
	//estado: { type: String, required: true },
	votos: [{ type: Schema.ObjectId, ref: 'Voto' }]
	//votos: [Voto.schema]
})

module.exports = mongoose.model('Idea', IdeaSchema)
