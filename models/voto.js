'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Idea = require('./idea')

const VotoSchema = Schema({
	//idea: { type: Schema.ObjectId, ref: 'Idea' },
	idea: { type: String },
	//userId: { type: Number, required: true }
	userId: { type: Number, index: { unique: true }, required: true }
})

module.exports = mongoose.model('Voto', VotoSchema)
