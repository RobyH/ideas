'use strict'

const Voto = require('../models/voto')
const Idea = require('../models/idea')

function getVotosByIdea(req, res){
	let ideaId = req.params.ideaId

	Voto.find({ idea : ideaId }, function(err, votos) {
		if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!votos) return res.status(404).send({message: `No existen votos`})		
		
		res.status(200).send({ votos });
	});
  // Obtiene los votos con su idea
	/*Voto.find({ idea : ideaId }).populate('idea').exec(function (err, votos) {
		if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!votos) return res.status(404).send({message: `No existen votos`})		
		
		//var count = votos.length
		res.status(200).send({ votos });
	});*/
}

function saveVoto(req, res){
	console.log('POST /api/voto')
	console.log(req.body)

	Idea.findById(req.body.idea, function(err,idea){
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!idea) return res.status(404).send({message: `La idea no existe`})
    var voto = new Voto()
    voto.idea = req.body.idea
    voto.userId = global.userId
    /*for(i in idea.votos){
      if(votos[i].userId == global.userId){
          //do something cool here
      }
    }*/
    idea.votos.push(voto)
    idea.save(function(err, data) {
      if(err) return res.status(500).send({message: `Error al guardar en la base de datos: ${err}`})
      res.status(200).send({message: 'Success', idea: data})
    });
  })

  /*Idea.find({ "votos.userId":  global.userId}, function(err, idea){
    if(err) res.status(500).send({message: `Error al realizar la peticion: ${err}`})
    
    res.status(200).send({message: 'Success', idea: idea})
  }).limit(1)

  Idea.update(,
     { $push: { translations: { _id: new ObjectId(), language: 'fr', text: 'French translation' } } }
  )
  WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
  }*/
}

function deleteVoto(req, res){
	let votoId = req.params.votoId;
  let ideaId = req.body.ideaId;
  //console.log(votoId)

  Idea.findOneAndUpdate(
    { _id: ideaId }, 
    { $pull: {votos: {_id: votoId}} },
    function(err, idea){
    	if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
	    else
      res.status(200).send({message: 'Success', idea: idea, ideaId:ideaId, votoId:votoId})
		}
  );

  /*Idea.findByIdAndUpdate(ideaId, {
    $pull: { votos: {_id: votoId}}
  }, function(err, idea){
  	if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
    
    res.status(200).send({message: 'Success', idea: idea})
  });*/

  /*Idea.findByIdAndUpdate(ideaId, function(err,idea){
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
    if(!idea) return res.status(404).send({message: `La idea no existe`})
    idea.votos.id(votoId).remove();
    //idea.votos.pull(votoId)
    idea.save(function(err, idea) {
      if(err) return res.status(500).send({message: `Error al guardar en la base de datos: ${err}`})
      res.status(200).send({message: 'Success', idea: idea})
    });
  })*/
}

module.exports = {
	getVotosByIdea,
	saveVoto,
	deleteVoto
}