'use strict'

const Idea = require('../models/idea')
const Voto = require('../models/voto')

function getIdea(req, res){
	let id = req.params.id

	Idea.findById(id, function(err, idea) {
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!idea) return res.status(404).send({message: `La idea no existe`})
    res.status(200).send({ idea })
	});

  /*Idea.findById(id, function (err, idea) {
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!idea) return res.status(404).send({message: `La idea no existe`})
    res.status(200).send({ idea })
  }).populate({ path:"votos", model:"Voto" });*/

  /*Idea.findById(id).populate({ path:"votos", model:"Voto" })
  .exec(function (err, idea) {
		if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!idea) return res.status(404).send({message: `La idea no existe`})
  	res.status(200).send({ idea });
  });*/

	/*Idea.aggregate([
		{ $unwind: '$votos' },
		//{ $match: { $_id: id } },
		{
			$group: {
				_id : {_id: '$_id', descripcion: '$descripcion'},
				total_votos: { $sum: 1 }
			}
		}
	], function (err, idea) {
    if (err) return res.status(500).send(err)
    res.status(200).send(idea)
	})*/
}

function getIdeas(req, res){
	
	Idea.find(function(err, ideas) {
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!ideas) return res.status(404).send({message: `La idea no existe`})
    res.status(200).send({ ideas })
	});

	/*Idea.aggregate([
    {
      $lookup: {
        from: "votos",
        localField: "_id",
        foreignField: "idea",
        as: "votos"
      }
    },
    {
      $project: {
        _id: 1,
        descripcion: 1,
        total_votos: { $size: "$votos" }
      }
    }
	], function(err, ideas){
    if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(!ideas) return res.status(404).send({message: `La idea no existe`})
    res.status(200).send({ ideas })
	})*/

}

function saveIdea(req, res){
	console.log('POST /api/idea')
	console.log(req.body)

	let idea = new Idea()
	idea.descripcion = req.body.descripcion
	//idea.autorId = req.body.autorId
	idea.autorId = global.userId
	idea.save(function(err, idea){
		if(err) return res.status(500).send({message: `Error al guardar en la base de datos: ${err}`})

		res.status(200).send({idea})
	})
}

function updateIdea(req, res){
	let id = req.params.id
	let update = req.body

	/*Idea.findByIdAndUpdate(id, update, function(err, idea){
		if(err) res.status(500).send({message: `Error al actualizar la idea: ${err}`})

		res.status(200).send({ idea: idea })
	})*/

	Idea.findById(id, function(err, idea){
		if(err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
		if(idea.autorId == global.userId){
			idea.descripcion = req.body.descripcion
			idea.save(function(err, idea){
				if(err) return res.status(500).send({message: `Error al actualizar la idea: ${err}`})

				res.status(200).send({idea})
			})
		}
		else
			return res.status(401).send({ message: 'No puedes modificar la idea de otro usuario' })
	})
}

function deleteIdea(req, res){
	let id = req.params.id

	Idea.findById(id, function(err, idea){
		if(err) return res.status(500).send({message: `Error al borrar la idea: ${err}`})
		if(idea.autorId == global.userId){
			idea.remove(function(err){
				if(err) return res.status(500).send({message: `Error al borrar la idea: ${err}`})
				res.status(200).send({ message: 'La idea ha sido eliminada'})
			})
		}
		else
			return res.status(401).send({ message: 'No puedes eliminar la idea de otro usuario' })
	})
}

module.exports = {
	getIdea,
	getIdeas,
	saveIdea,
	updateIdea,
	deleteIdea
}