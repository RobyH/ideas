'use strict'

const jwt = require('jsonwebtoken')
//const moment = require('moment')
const config = require('../config')

function isAuth(req, res, next)
{
	if(!req.headers.authorization)
	{
		return res.status(403).send({
			ok: false,
      		message: 'Acceso no autorizado'
		})
	}

	const token = req.headers.authorization
	//const payload = jwt.decode(token, '2ErzSngye7B0a0M0xjEsIJKl0bU8InUZ');

	jwt.verify(token, '2ErzSngye7B0a0M0xjEsIJKl0bU8InUZ', function(err, token) {
    if (err) {
      return res.status(401).send({
        ok: false,
        message: 'Token inválido'
      });
    } else {
    	req.token = token
    	//req.userId = token.sub
    	global.userId = token.sub
		next()
    }
  });
}

module.exports = isAuth