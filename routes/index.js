'use strict'

const express = require('express')
const ideaCtrl = require('../controllers/idea')
const votoCtrl = require('../controllers/voto')
const auth = require('../middlewares/auth')
const api = express.Router()

api.get('/ideas', auth, ideaCtrl.getIdeas)
api.get('/ideas/:id', auth, ideaCtrl.getIdea)
api.post('/ideas', auth, ideaCtrl.saveIdea)
api.put('/ideas/:id', auth, ideaCtrl.updateIdea)
api.delete('/ideas/:id', auth, ideaCtrl.deleteIdea)

api.get('/votos/:ideaId', votoCtrl.getVotosByIdea)
api.post('/votos', auth, votoCtrl.saveVoto)
api.delete('/votos/:votoId', votoCtrl.deleteVoto)

module.exports = api